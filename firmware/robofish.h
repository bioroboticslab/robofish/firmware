/***** ROBOT IDENTIFICATION *****/
/* change this before uploading */

#define ROBOT_ID 120

// Assuming 8V == 100% and 6V == 0%
// Calculate upper PWM according to Voltage Divider formula:
// Vo = (V*R2)/(R1+R2),  R2 = 1k,  R1 = 10k
// Vo_min = 0,545
// Vo_max = 0,818

#if ROBOT_ID == 101
#define BATT_VAL_6V 545
#define BATT_VAL_9V 727
#define MOTOR_VOLT 5.00

#elif ROBOT_ID == 102
#define BATT_VAL_6V 532
#define BATT_VAL_9V 710
#define MOTOR_VOLT 5.00

#elif ROBOT_ID == 107 // HU-Bot fixed up with prototype HW
#define BATT_VAL_6V 545
#define BATT_VAL_9V 727
#define MOTOR_VOLT 5.00
#elif ROBOT_ID == 108 // new NodeMCU Bot
#define BATT_VAL_6V 545
#define BATT_VAL_9V 727
#define MOTOR_VOLT 5.00
#elif ROBOT_ID >= 110 && ROBOT_ID<=119 // NodeMCU + PCB
#define BATT_VAL_6V 545
#define BATT_VAL_9V 727
#define MOTOR_VOLT 5.00
#elif ROBOT_ID >= 120 // NodeMCU + PCB V2.1
#define BATT_VAL_6V 545
#define BATT_VAL_9V 727
#define MOTOR_VOLT 5.00

#else
#error "No parameters #define'd for this ROBOT_ID"
#endif
