/*
 * UDP endpoint
 *
 * A simple UDP endpoint example using the WiShield 1.0
 */

#include <limits>

#include <SPI.h>
#include <ESP8266WiFi.h>
#include <WiFiUdp.h>

ADC_MODE(ADC_TOUT)

extern "C" { // required for read Vdd Voltage
#include "user_interface.h"
}

#include <Arduino.h>
#include "robofish.h"
#include "debug.h"
#include "wifi_config.h"
#include "consts.h"
#include "Packet.h"
#include "modulo.h"

// Battery
int    batteryValue       = 0;
double currentBattVoltage = 0.0;

// Speeds
uint16_t speedLeft  = 0;
uint16_t speedRight = 0;
uint8_t  directions = 0x00;

WiFiUDP       Udp;
int           status = WL_IDLE_STATUS; // the Wifi radio's status
char          packetBuffer[255];       // buffer to hold incoming packet
char          replyBuffer[255];        // buffer to hold incoming packet
unsigned long lastPacketAt =
   0; // time in milliseconds the last packet arrived at

void sendVoltage()
{
	// We need to use this SDK function for reading the ADC.
	// analog_read from Arduino SKD only delivers binary values 0 or 1024.
	batteryValue = system_adc_read();
	// Debug out:
	// Serial.println(batteryValue);

	// Previously this formula was used. By logic this should work, but does not
	// reflect the property of the voltage divider properly.
	// currentBattVoltage = 6.0 + (batteryValue - BATT_VAL_6V) * 3.0 /
	// (BATT_VAL_9V - BATT_VAL_6V);

	// (we assume 8V == 100% and 6V == 0%, but the GUI makes the actual mapping)
	// Measured value from ADC is p and k is the mapping from read value to
	// actual voltage. k was experimentally found, as the output does not scale
	// linearly as expected from 1..1024 to 0V..1V Calculate upper PWM according
	// to Voltage Divider formula:
	//  Vo = (V*R2)/(R1+R2),  R2 = 1k,  R1 = 10k, k = 1064/1000
	//  Vo = p * k
	//  p * k = (V*R2)/(R1+R2)
	//  p * k * (R1+R2) = V*R2
	//  (p * k * (R1+R2)) / R2 = V
	// Note: Accuracy seems to be +/- 0.05V. Tendency is to -0.04V
	float k            = 1064.0 / 1000.0;
	float r1           = 10000.0;
	float r2           = 1000.0;
	float p            = ((float) batteryValue) / 1000.0;
	currentBattVoltage = ((p * (r1 + r2)) / r2) * k;

	writeVoltageRePacket(replyBuffer, currentBattVoltage);

	// send a reply, to the IP address and port that sent us the packet we
	// received
	Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
	Udp.write(replyBuffer, sizeof(Packet));
	Udp.endPacket();
	V_DBG_MSG_LN("Voltage packet send");
}

void inline sendTimingReply()
{
	writeTimingRePacket(
	   replyBuffer); // send a reply, to the IP address and port that sent us
	                 // the packet we received
	Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
	Udp.write(replyBuffer, sizeof(Packet));
	Udp.endPacket();

	V_DBG_MSG_LN("Timing packet send");
}

void inline sendDiscoveryReply()
{
	writeDiscoveryRePacket(
	   replyBuffer,
	   ROBOT_ID); // send a reply, to the IP address and port that sent us the
	              // packet we received
	Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
	Udp.write(replyBuffer, sizeof(Packet));
	Udp.endPacket();

	V_DBG_MSG_LN("Discovery packet reply send");
}

void setup()
{
	pinMode(D1_, OUTPUT);
	pinMode(D2_, OUTPUT);
	digitalWrite(D1_, LOW); // LOW = clockwise
	digitalWrite(D2_, LOW); // HIGH = counter-clockwise
	analogWrite(M1_, 0);
	analogWrite(M2_, 0);

	Serial.begin(115200);
	DBG_MSG_LN("");
	DBG_MSG_LN("(Baud switched)");
	DBG_MSG_LN("Starting setup");

	// attempt to connect to Wifi network:

	status = WiFi.begin(ssid, pass);
	DBG_MSG_LN("Init...");

	// Wait for connection
	while (WiFi.status() != WL_CONNECTED) {
		delay(500);
		Serial.print(".");
	}
	DBG_MSG_LN(WiFi.localIP());

	DBG_MSG_LN("WLAN initialization complete");

	// analogReference(DEFAULT); TODO ???
	pinMode(BatteryPort, INPUT);

	setMotorPWM(false, 0, false, 0);

	DBG_MSG_LN("Setup complete");

	Udp.begin(12344);
}

void loop()
{
	// if there's data available, read a packet
	int packetSize = Udp.parsePacket();
	if (packetSize) {
		lastPacketAt = millis();

		// read the packet into packetBufffer
		int len = Udp.read(packetBuffer, 255);
		if (len > 0) {
			packetBuffer[len] = 0;
		}

		Packet* packet = (Packet*) packetBuffer;

		// V_DBG_MSG_LN("Packet recieved");

		// Examine packet
		if (packet->type == TimingRequest) {
			// V_DBG_MSG_LN("Received packet contains timing request");
			sendTimingReply();
		} else if (packet->type == MotorRequest) {
			// V_DBG_MSG_LN("Received packet contains motor request");
			speedLeft = packet->data.motor.left[0] +
			            packet->data.motor.left[1] * 256;
			speedRight = packet->data.motor.right[0] +
			             packet->data.motor.right[1] * 256;
			directions = packet->data.motor.directions;
		} else if (packet->type == DiscoveryRequest) {
			// V_DBG_MSG_LN("Discovery packet received");
			sendDiscoveryReply();
		} else if (packet->type == VoltageRequest) {
			// V_DBG_MSG_LN("Received packet contains status request");
			sendVoltage();
		} else {
			V_DBG_MSG_LN("Received gibberish packet");
		}
	} else if (speedLeft != 0 || speedRight != 0) {
		auto const now = static_cast<long long>(millis());
		auto const dt  = now - static_cast<long long>(lastPacketAt);
		auto const n   = 1 + static_cast<long long>(
                            std::numeric_limits<unsigned long>::max());
		if (1000 < modulo(dt, n)) {
			speedLeft  = 0;
			speedRight = 0;
			directions = 0;
		}
	}

	setMotorPWM(directions & LeftReverse,
	            speedLeft,
	            directions & RightReverse,
	            speedRight);
}

void setMotorPWM(bool     leftReverse,
                 uint16_t speedLeft,
                 bool     rightReverse,
                 uint16_t speedRight)
{
	digitalWrite(D1_, leftReverse ? HIGH : LOW);
	digitalWrite(D2_, rightReverse ? LOW : HIGH);

	uint16_t pwmLeft  = (uint16_t) speedLeft;
	uint16_t pwmRight = (uint16_t) speedRight;

	// The ESP12F is doing software PWM.
	// This can be flaky at the boundaries, so better round it then.
	// if (pwmLeft >= 1024)
	//    pwmLeft = 1023;
	// if (pwmLeft < 0)
	//    pwmLeft = 0;
	// if (pwmRight > 1024)
	//    pwmRight = 1023;
	// if (pwmRight < 0)
	//    pwmRight = 0;

	// Right now everything is geared towards arduino 8-bit PWM.
	// So we simply scale this up
	analogWrite(M1_, pwmLeft);
	analogWrite(M2_, pwmRight);
}
