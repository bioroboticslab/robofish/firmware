#define DEBUG_LEVEL 2 // 0: silent, 1: some output, 2: verbose output

#if DEBUG_LEVEL > 0
#define DBG_MSG_LN(s) Serial.println(s)
#define DBG_MSG(s) Serial.print(s)
#else
#define DBG_MSG_LN(s)
#define DBG_MSG(s)
#endif

#if DEBUG_LEVEL > 1
#define V_DBG_MSG_LN(s) Serial.println(s)
#define V_DBG_MSG(s) Serial.print(s)
#else
#define V_DBG_MSG_LN(s)
#define V_DBG_MSG(s)
#endif
