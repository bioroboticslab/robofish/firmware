#include <Arduino.h>
#include "robofish.h"

// const int M1 = 5;         // Arduino PWM Speed Control
// const int D1 = 2;         // Arduino Digital Direction Control
// const int M2 = 6;         // Arduino PWM Speed Control
// const int D2 = 8;         // Arduino Digital Direction Control
#define NODEMCU 1

#ifdef NODEMCU
// NodeMCU Mapping
static const uint8_t MCU_D0__  = 16;
static const uint8_t MCU_D1__  = 5;
static const uint8_t MCU_D2__  = 4;
static const uint8_t MCU_D3__  = 0;
static const uint8_t MCU_D4__  = 2;
static const uint8_t MCU_D5__  = 14;
static const uint8_t MCU_D6__  = 12;
static const uint8_t MCU_D7__  = 13;
static const uint8_t MCU_D8__  = 15;
static const uint8_t MCU_D9__  = 3;
static const uint8_t MCU_D10__ = 1;
#endif

// GPIO4 == 19
// GPIO5 == 20
#ifdef NODEMCU
// D3=Dir1
// D4=Forw1
// D7=Dir2
// D8=Forw2
const int M1_ = MCU_D3__;  // Arduino PWM Speed Control
const int D1_ = MCU_D4__;  // Arduino Digital Direction Control
const int M2_ = MCU_D8__;  // Arduino PWM Speed Control
const int D2_ = MCU_D7__;  // Arduino Digital Direction Control
#define ANALOG_PORT_BAT A0 // A0
#else
const int M1_ = 12; // Arduino PWM Speed Control
const int D1_ = 13; // Arduino Digital Direction Control
const int M2_ = 14; // Arduino PWM Speed Control
const int D2_ = 16; // Arduino Digital Direction Control
#define A2 2
#define ANALOG_PORT_BAT A2 // A2
#endif

// const int LedPin  = 2;    // LED output pin
// const int LedPort = 9;

const int    BatteryPort = ANALOG_PORT_BAT;
const double BatteryMax  = 8.3;
