#pragma once

// MSVC up to version 2008 do not ship with stdint.h
// (which defines uintX_t etc.), so we need to include
// one, e.g. from https://code.google.com/p/msinttypes/
#if defined _MSC_VER && _MSC_VER < 1600
#include "stdint.h"
#else
#if defined _MSC_VER
//#define uint8_t unsigned __int8
#endif
#include <stdint.h>
#endif

#include <stdint.h>

#include <stdlib.h>

const uint8_t protocolVersion = 2;

// Packet types
const char DiscoveryRequest = 'D';
const char MotorRequest     = 'M';
const char TimingRequest    = 'T';
const char VoltageRequest   = 'V';
const char LedOnRequest     = 'L';
const char BoostCfgRequest  = 'B';
const char DiscoveryReply   = 'd'; // deprecated, only used by old protocol
const char DiscoveryVersionReply = 'e'; // new protocol reply
const char TimingReply           = 't';
const char VoltageReply          = 'v';

// Bit set -> reverse motor
const uint8_t LeftReverse  = 0x01;
const uint8_t RightReverse = 0x02;

// Set memory alignment to one byte so that this struct
// is really the same on all platforms
#ifdef _MSC_VER
#pragma pack(push, 1)
#endif

struct Packet
{
	char type;
	union {
		struct
		{
			uint8_t directions;
			uint8_t left[2];
			uint8_t right[2];
		} motor;

		char    voltStr[3];
		uint8_t robotID;
		struct
		{
			uint8_t robotID;
			uint8_t version;
		} discovery;

	} data;
}
#if defined __GNUC__ || defined __MINGW32__
__attribute__((packed, aligned(1)))
#endif
;

void inline writeDiscoveryRqPacket(void* dest)
{
	Packet* p = (Packet*) dest;
	p->type   = DiscoveryRequest;
}

void inline writeMotorRqPacket(void*   dest,
                               int32_t leftSpeed,
                               int32_t rightSpeed)
{
	Packet* p                = (Packet*) dest;
	p->type                  = MotorRequest;
	p->data.motor.directions = (leftSpeed > 0 ? LeftReverse : 0x00) |
	                           (rightSpeed > 0 ? RightReverse : 0x00);
	rightSpeed             = abs(rightSpeed);
	leftSpeed              = abs(leftSpeed);
	p->data.motor.left[0]  = (uint8_t)(leftSpeed % 256);
	p->data.motor.left[1]  = (uint8_t)(leftSpeed / 256);
	p->data.motor.right[0] = (uint8_t)(rightSpeed % 256);
	p->data.motor.right[1] = (uint8_t)(rightSpeed / 256);
}

void inline writeTimingRqPacket(void* dest)
{
	Packet* p = (Packet*) dest;
	p->type   = TimingRequest;
}

void inline writeVoltageRqPacket(void* dest)
{
	Packet* p = (Packet*) dest;
	p->type   = VoltageRequest;
}

void inline writeLedOnRq(void* dest)
{
	Packet* p = (Packet*) dest;
	p->type   = LedOnRequest;
}

void inline writeDiscoveryRePacket(void* dest, uint8_t robotID)
{
	Packet* p                 = (Packet*) dest;
	p->type                   = DiscoveryVersionReply;
	p->data.discovery.robotID = robotID;
	p->data.discovery.version = protocolVersion;
}

void inline writeTimingRePacket(void* dest)
{
	Packet* p = (Packet*) dest;
	p->type   = TimingReply;
}

void inline writeVoltageRePacket(void* dest, double voltage)
{
	Packet* p = (Packet*) dest;
	p->type   = VoltageReply;
	char voltBuf[4];
	;
	snprintf(voltBuf, 4, "%f", voltage);
	p->data.voltStr[0] = voltBuf[0];
	p->data.voltStr[1] = voltBuf[2];
	p->data.voltStr[2] = voltBuf[3];
}

#ifdef _MSC_VER
#pragma pack(pop)
#endif
