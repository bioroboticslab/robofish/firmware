#pragma once

inline long long floored_remainder(long long a, long long n)
{
	auto const r = a % n;
	if (r && (a < 0) != (n < 0))
		return r + n;
	return r;
}

inline long long modulo(long long a, long long n)
{
	if (a > 0) {
		return a % n;
	} else if (n > 0) {
		return floored_remainder(a, n);
	} else {
		return -floored_remainder(-a, n);
	}
}
