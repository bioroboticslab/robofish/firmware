#!/usr/bin/env python2

from ctypes import *
import time
import socket

DISCOVERY_REQUEST = 'D'
MOTOR_REQUEST = 'M'
TIMING_REQUEST = 'T'
VOLTAGE_REQUEST = 'V'
LEDON_REQUEST = 'L'
BOOSTCFG_REQUEST = 'B'

DISCOVERY_REPLY = 'd'
TIMING_REPLY = 't'
VOLTAGE_REPLY = 'v'

BOOST_OFF = 'O'
BOOST_PUSH = 'P'
BOOST_INTERLACE = 'I'

LEFT_REVERSE = 0x01
RIGHT_REVERSE = 0x02

class MOTOR(Structure):
    _pack_ = 1
    _fields_ = [("directions", c_int8),
                ("left", c_int8),
                ("right", c_int8)]

class BOOST(Structure):
    _pack_ = 1
    _fields_ = [("method", c_char),
                ("threshold", c_int8),
                ("boostVal", c_int8),
                ("cycles", c_int8)]

class DATA(Union):
    _pack_ = 1
    _fields_ = [("motor",  MOTOR),
                ("voltStr", c_char * 3),
                ("robotID", c_int8),
                ("boost", BOOST)]

class PACKET(Structure):
    _pack_ = 1
    _fields_ = [("type", c_char),
                ("data", DATA)]

def discovery_request():
    return PACKET(DISCOVERY_REQUEST)

def motor_request(leftSpeed, rightSpeed):
    return PACKET(MOTOR_REQUEST, DATA(motor=MOTOR(
        (LEFT_REVERSE if leftSpeed < 0 else 0x00)
            | (RIGHT_REVERSE if rightSpeed < 0 else 0x00),
        min(abs(leftSpeed), 255),
        min(abs(rightSpeed), 255)
    )))

def timing_request():
    return PACKET(TIMING_REQUEST)

def voltage_request():
    return PACKET(VOLTAGE_REQUEST)

def ledon_request():
    return PACKET(LEDON_REQUEST)

def boostcfg_request(method, threshold=0, val=0, cycles=0):
    return PACKET(BOOSTCFG_REQUEST, DATA(boost=BOOST(
        method,
        threshold,
        val,
        cycles
    )))

def _find_getch():
    try:
        import msvcrt
        return msvcrt.getch
    except ImportError:
        def _getch():
            """
            Gets a single character from STDIO.
            """
            import sys
            import tty
            import termios
            fd = sys.stdin.fileno()
            old = termios.tcgetattr(fd)
            try:
                tty.setraw(fd)
                return sys.stdin.read(1)
            finally:
                termios.tcsetattr(fd, termios.TCSADRAIN, old)
        return _getch

getch = _find_getch()

IP_PREFIX = "192.168.0."
PORT = 12344

def loop(id, sock, addr):

    ROBOT_ID = id;

    leftSpeed = 0
    rightSpeed = 0

    print IP_PREFIX+str(ROBOT_ID)

    while True:

        speedChanged = False

        command = getch()

        if command == 'w':
            speedChanged = True
            leftSpeed += 10
            rightSpeed += 10
        elif command == 's':
            speedChanged = True
            leftSpeed -= 10
            rightSpeed -= 10
        elif command == 'a':
            speedChanged = True
            leftSpeed -= 5
            rightSpeed += 5
        elif command == 'd':
            speedChanged = True
            leftSpeed += 5
            rightSpeed -= 5
        elif command == 'm':
            speedChanged = True
            leftSpeed = 255
            rightSpeed = 255
        elif command == 'n':
            speedChanged = True
            leftSpeed = 0
            rightSpeed = 0
        elif command == 'v':
            while True:
                sock.sendto(voltage_request(), (IP_PREFIX+str(ROBOT_ID), PORT));
                try:
                    packet = PACKET()
                    nbytes, new_packet_addr = sock.recvfrom_into(packet)

                    if new_packet_addr[0] == IP_PREFIX+str(ROBOT_ID):
                        if packet.type == VOLTAGE_REPLY:
                            print "Voltage: "+packet.data.voltStr
                            break;
                        else:
                            pass
                    else:
                        print "Packet from "+new_packet_addr[0]+". Ignoring"
                except socket.timeout:
                    return;
        elif command == 'p':
            sock.sendto(boostcfg_request(BOOST_PUSH, 8, 35, 20), (IP_PREFIX+str(ROBOT_ID), PORT));
        elif command == 'i':
            sock.sendto(boostcfg_request(BOOST_INTERLACE, 16), (IP_PREFIX+str(ROBOT_ID), PORT));
        elif command == 'o':
            sock.sendto(boostcfg_request(BOOST_OFF), (IP_PREFIX+str(ROBOT_ID), PORT));
        elif command == 'e':
            sock.sendto(boostcfg_request(BOOST_OFF), (IP_PREFIX+str(ROBOT_ID), PORT));
            sock.sendto(motor_request(0, 0), (IP_PREFIX+str(ROBOT_ID), PORT));
            return;
        else:
            print "Invalid Command"
            continue;

        if speedChanged:
            print "speed: leftSpeed: "+str(leftSpeed)+", rightSpeed: "+str(rightSpeed)
            sock.sendto(motor_request(leftSpeed, rightSpeed), (IP_PREFIX+str(ROBOT_ID), PORT));


def main():
    try:
        ROBOT_ID = int(input("Enter ROBOT_ID: "))
    except:
        print "Could not parse as Integer"
        return

    sock = socket.socket(socket.AF_INET, # Internet
                     socket.SOCK_DGRAM) # UDP
    sock.bind(("", PORT))
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.settimeout(5.0)

    print "Searching for Robot "+str(ROBOT_ID)
    while True:
        sock.sendto(discovery_request(), (IP_PREFIX+str(ROBOT_ID), PORT));
        try:
            packet = PACKET()
            nbytes, addr = sock.recvfrom_into(packet)

            if packet.type == DISCOVERY_REPLY:
                if packet.data.robotID == ROBOT_ID:
                    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 0)

                    print "Connected"
                    print ""
                    print "Commands:"
                    print " w: Increase Speed"
                    print " s: Decrease Speed"
                    print " a: Increase Left Turn"
                    print " d: Increase Right Turn"
                    print " m: Max Speed"
                    print " n: No Speed"
                    print " v: Voltage Request"
                    print " p: Boost Push"
                    print " i: Boost Interlaced"
                    print " o: Boost Off"
                    print " e: Exit"
                    print ""

                    # not working right now
                    #sock.sendto(ledon_request(), (IP_PREFIX+str(ROBOT_ID), PORT));
                   
                    # used for quick distance measuring
                    #print "Start" 
                    #sock.sendto(motor_request(255,255), (IP_PREFIX+str(ROBOT_ID), PORT));
                    #time.sleep(1.0)
                    #sock.sendto(motor_request(0, 0), (IP_PREFIX+str(ROBOT_ID), PORT));
                    #print "Stop"
                    
                    loop(ROBOT_ID, sock, addr)

                    print ""
                    print "Disconnected"
                    return

                else:
                    print "Discovered Robot "+str(robot.data_robotID)+". Ignoring"
            else:
                pass
        except socket.timeout:
            pass
        time.sleep(5.0)

if __name__ == "__main__":
    main()
