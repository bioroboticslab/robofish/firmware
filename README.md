# Flashing the firmware

- Install the Arduino IDE 1.8.19 (see https://git.imp.fu-berlin.de/bioroboticslab/robofish/firmware/-/issues/1 for an issue with Arduino IDE 2.0.0)
- Install the [drivers for the nodemcu](https://github.com/nodemcu/nodemcu-devkit/blob/master/Drivers/CH341SER_WINDOWS.zip)
- Arduino IDE -> File -> Settings   
  Set Additional Boards Manager URLS:  
  http://arduino.esp8266.com/stable/package_esp8266com_index.json
- Arduino IDE -> Tools -> Boards Manager
  Search for "esp", install "esp8266" (tested version is 2.5.0-beta3)
- Make sure the settings are as depicted:  
  ![Menu](Menu.png)
- Put the robot's ID in robofish.h and remember not to commit your changes to git:  
  #define ROBOT_ID 112
- Adjust the code as needed (e.g. wifi password)
- Upload the code

# Robot design

Please refer to the [wiki](https://git.imp.fu-berlin.de/bioroboticslab/robofish/hardware/-/wikis/home)